import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x, y;

        System.out.println("Input x and y:");
        x = scanner.nextDouble();
        y = scanner.nextDouble();


        if(x == 0 && y == 0)
            System.out.println("На пересечении осей");

        else if (x == 0 || y == 0)
            System.out.println("Ось " + (x == 0 ? "X" : "Y"));

        else if (x > 0) {
            if (y > 0)
                System.out.println("I");
            else
                System.out.println("IV");
        }

        else if (x < 0) {
            if (y > 0)
                System.out.println("II");
            else
                System.out.println("III");
        }
    }
}
